#!/bin/bash 
URL=https://api.exchangeratesapi.io/latest
MOEDA_ORIGEM=${1^^}
MOEDA_DESTINO=${2^^}
VALOR=$3


URL_COMPLETA=${URL}?base=${MOEDA_ORIGEM}'&'symbols=${MOEDA_DESTINO}

RATE=`curl ${URL_COMPLETA} | jq '.rates.'${MOEDA_DESTINO}` 


echo `python -c 'print('${VALOR}'*'${RATE}')'`
